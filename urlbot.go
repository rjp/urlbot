package main

import (
    "fmt"
    "flag"
    "strconv"
    "strings"
    "encoding/json"

    "bitbucket.org/rjp/gokwuan"
    "github.com/mvdan/xurls"
    "github.com/garyburd/redigo/redis"
)

type UrlMetadata struct {
	Canonical string
	ContentType string
	Description string
	Image string
	Keywords string
	MessageID int
	Title string
	URL string
}
type Url struct {
    URL string
    MessageID int
    siblings int
}

// Bit kludgey but it's nice to take advantage of strict typing
type InOut int
const (
    In InOut = iota
    Out
)

// 
type UrlPacket struct {
    url Url
	metadata UrlMetadata
    direction InOut
}

var emptyMeta UrlMetadata = UrlMetadata{}
var emptyUrl  Url = Url{}

// Fetch a single message from the server via the helper method
func requestMessage(event gokwuan.ClientEvent) {
    h := event.Data.ValueMap()
    messageid, _ := strconv.Atoi(h["messageid"])
    fmt.Println("Requesting",messageid)
    event.Bot.RequestMessage(messageid)
}

// Extract URLs from a chunk of text hopefully including
// hard-wrapped ones.
func parseOutURLs(text string, messageid int) ([]Url) {
    wrapped := make(map[string]int)
    uniques := make(map[string]int)
    urls := []Url{}

    found := xurls.Strict.FindAllString(text, -1)
    for _, url := range found {
        if len(url) == 79 {
            fmt.Println(url, "is suspiciously sized.")
            wrapped[url] = 1
        } else {
            uniques[url] = 1
        }
    }

    if len(wrapped) > 0 {
        for url, _ := range wrapped {
            fmt.Println("Removing wrap from", url)
            text = strings.Replace(text, url + "\n", url, -1)
        }

        found := xurls.Strict.FindAllString(text, -1)
        for _, url := range found {
            uniques[url] = 1
        }
    }

    for url, _ := range uniques {
        urls = append(urls, Url{url, messageid, len(uniques)})
    }

    return urls
}

func handleMessage(event gokwuan.ClientEvent) {
    // This should distinguish between "many" and "one"
    t, e := event.Data.ValueOfTree("reply.message")
    if e != nil { panic(e) }
    h := t.ValueMap()

    // Annoyingly, the id is from `<message=1234>` rather than `<messageid=1234/>`
    messageid, _ := strconv.Atoi(h["message"])
    urls := parseOutURLs(h["text"], messageid)

    for _, urlblob := range urls {
        urlblob.siblings = len(urls)
        event.Bot.Channels["urls"] <- urlblob
    }
}

func newHandleUrls(bot gokwuan.Client, blob interface{}, data interface{}) bool {
    url := data.(Url)
    fmt.Println("GOTTA EARL", url.MessageID, "", url.URL)
    bot.Channels["micro"] <- UrlPacket{url, emptyMeta, Out}
    // Never quits
    return false
}

func handleUrls(bot gokwuan.Client) {
    defer bot.ExtraWG.Done()

    for {
        abstractURL := <-bot.Channels["urls"]
        url := abstractURL.(Url)
        fmt.Println("GOTTA EARL", url.MessageID, "", url.URL)
        bot.Channels["micro"] <- UrlPacket{url, emptyMeta, Out}
    }
}

// Because maps aren't concurrent-safe (indeed, we'll panic if we
// try to use them like that and go notices) we 'synchronise' our
// access by consolidating all our "how many URLs" counting into
// a single goroutine - which is, of course, not concurrent with itself.
func urlMicroServiceTransport(bot gokwuan.Client) {
    defer bot.ExtraWG.Done()

    counters := make(map[int]int)

    for {
        absPacket := <-bot.Channels["micro"]

        packet := absPacket.(UrlPacket)

        switch packet.direction {
            case In: {
				if packet.metadata == emptyMeta {
					panic("Bit weird, got a nil metadata")
				}
				metadata := packet.metadata
				fmt.Println("Got a blob back from uS", metadata)
			}
            case Out: {
                // missing hash element returns the zero value
                counters[packet.url.MessageID] = counters[packet.url.MessageID] + 1
                fmt.Println("Sending a blob to uS", counters[packet.url.MessageID])
                bot.Channels["redisWriter"] <- packet.url
            }
        }
    }
}

func handleEvents(bot gokwuan.Client, event gokwuan.ClientEvent) bool {
    switch event.Event {
        case "announce_message_add": requestMessage(event)
        case "reply_message_list": handleMessage(event)
        default: fmt.Println("EVENT:", event.Event)
    }
    return false
}

func redisConnInit(c gokwuan.Client, channel string) interface{} {
    r_conn, err_w := redis.Dial("tcp", ":6379")
    if err_w != nil {
        panic(err_w)
    }

    return r_conn
}

// Accepts strings on a channel and sends them to Redis PUBSUB
// NOTE I'm not convinced all these goroutines should be adding
// to the bot's waitgroup instead of a local one.  On the other
// hand, we use the bot object to pass around our channels.
func redisWriter(bot gokwuan.Client, init interface{}, data interface{}) bool {
    r_writer := init.(redis.Conn)

    sendME := data.(Url)
    sendJSON, _ := json.Marshal(sendME)
    fmt.Println("PUBSUB", sendME)
    r_writer.Do("publish", "url-metadata", sendJSON)

    return false
}

func redisReader(bot gokwuan.Client) {
    r_reader := redisConnInit(bot, "").(redis.Conn)
	var url_message UrlMetadata

	pubsub := redis.PubSubConn{r_reader}
    pubsub.Subscribe("url-metadata")

    for {
        url := ""
        switch v := pubsub.Receive().(type) {
        case redis.Message:
            println("-> ", v.Channel)
            if v.Channel == "url-metadata" {
                err := json.Unmarshal(v.Data, &url_message)
                if err != nil {
                    panic(err)
                }
                println("id->", url_message.MessageID)
                url = url_message.URL
                // url_response.MessageID = url_message.MessageID
            }
        case redis.Subscription:
            // ignore
        case error:
            panic(v)
        }
        // Didn't get anything from the Redis message
        if len(url) == 0 {
            continue
        }
		fmt.Println("URL DATA", url_message)
		bot.Channels["micro"] <- UrlPacket{emptyUrl, url_message, In}
    }
}

func main() {
    var username = flag.String("u", "", "-u username")
    var password = flag.String("p", "", "-p password")
    flag.Parse()

    if *username == "" || *password == "" {
        panic("Need a username or password")
    }

    bot := gokwuan.New(*username, *password)

    urls := make(chan interface{}, 8)
    bot.Channels["urls"] = urls

    uS := make(chan interface{}, 8)
    bot.Channels["micro"] = uS

    // bot.Channel("redisWriter", redisWriter)
    bot.SpawnChannel("redisWriter", redisConnInit, redisWriter)
    bot.Spawn(redisReader)
    bot.Spawn(urlMicroServiceTransport)
    bot.SpawnChannel("urls", nil, newHandleUrls)
//    bot.Spawn(handleUrls)
    bot.EventHandler(handleEvents)

    // Never returns
    bot.Login()
}
