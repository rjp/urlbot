# URLBOT

The old Ruby-based bot is unreliable - requests either don't make
it to the server or the replies are missed.

Example JSON we send/receive over Redis PUBSUB.

    {
        "Canonical": "http://www.bbc.co.uk/sport/0/football/35175829",
        "ContentType": "text/html; charset=utf-8",
        "Description": "\\u003e After Manchester City's new badge design is leaked, fans have\\n\\u003e been having their say on what they think of the new crest.",
        "Image": "http://newsimg.bbc.co.uk/media/images/67165000/jpg/_67165916_67165915.jpg",
        "Keywords": "BBC, Sport, BBC Sport, bbc.co.uk, world, uk, international, foreign, british, online, service",
        "MessageID": 2286272,
        "Tags": "[]",
        "Title": "'It looks like the Blue Peter badge'",
        "URL": "http://www.bbc.co.uk/sport/0/football/35175829"
    }
